# Tracy environment panel
Custom Tracy panel for show environment variables

## Requirements
- [tracy/tracy](https://github.com/nette/tracy) 2.4+
- php 5.4+

## Installation
Use [Composer](http://getcomposer.org/) to install package:
```sh
$ composer require janharsa/tracy-environment-panel:~1.0

```

```neon
tracy:
    bar:
        - JanHarsa\Tracy\EnvironmentPanel
```